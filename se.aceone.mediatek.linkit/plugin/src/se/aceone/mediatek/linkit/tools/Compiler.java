package se.aceone.mediatek.linkit.tools;

import org.eclipse.cdt.core.envvar.IContributedEnvironment;
import org.eclipse.cdt.core.settings.model.ICConfigurationDescription;
import org.eclipse.cdt.core.settings.model.ICProjectDescription;
import org.eclipse.cdt.core.settings.model.ICResourceDescription;

import se.aceone.mediatek.linkit.common.LinkItConst;

public abstract class Compiler implements LinkItConst {

	protected LinkItHelper helper;

	public Compiler() {
	}
	
	protected void setLinkItHelper(LinkItHelper helper) {
		this.helper = helper;
	}
	
	abstract public String getCompilerPath();

	abstract public void setMacros(ICProjectDescription projectDescription);

	abstract void setIncludePaths(ICProjectDescription projectDescriptor, IContributedEnvironment contribEnv, ICConfigurationDescription configuration);

	abstract String getToolChainId();

	abstract String getToolPath() ;

	abstract String getName();

}
