package se.aceone.mediatek.linkit.tools;

import org.eclipse.cdt.core.envvar.EnvironmentVariable;
import org.eclipse.cdt.core.envvar.IContributedEnvironment;
import org.eclipse.cdt.core.settings.model.ICConfigurationDescription;
import org.eclipse.cdt.core.settings.model.ICProjectDescription;
import org.eclipse.cdt.core.settings.model.ICSettingEntry;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;

public class CompilerGCC extends Compiler {

	public static final String COMPILER_IT_SDK10 = "ARMCOMPILER";

	@Override
	public String getCompilerPath() {
		String compiler = System.getenv().get(COMPILER_IT_SDK10);
		if (compiler == null) {
			compiler = "C:\\dev\\eclipseMTK\\LINKIT_ASSIST_SDK\\tools\\gcc-arm-none-eabi-4_9-2014q4-20141203-win32\\";
		}
		return compiler;
	}

	@Override
	public void setMacros(ICProjectDescription projectDescription) {
		helper.addMacro(projectDescription, "__GNUC__", null, ICSettingEntry.BUILTIN);
	}

	protected final String getToolChainId() {
		switch (helper.getOutputType().intValue()) {
		case 0: // VXP
			return LINKIT_DEFAULT_TOOL_CHAIN_GCC;
		case 3: // Static
			// TODO tool chain don exist, fix it later
			return LINKIT_DEFAULT_TOOL_CHAIN_STATIC_GCC;
		}
		return null;
	}

	@Override
	String getToolPath() {
		return new Path(COMPILER_TOOL_PATH_GCC).toPortableString();
	}
	
	
	@Override
	void setIncludePaths(ICProjectDescription projectDescriptor, IContributedEnvironment contribEnv, ICConfigurationDescription configuration) {
		IPath compilerLocation = new Path(getCompilerPath());
		IPath armIncl = compilerLocation.append("arm-none-eabi/include");
		helper.addIncludeFolder(projectDescriptor, armIncl);
		armIncl = armIncl.append("c++/4.9.3");
		helper.addIncludeFolder(projectDescriptor, armIncl);
		IPath armThumb = armIncl.append("arm-none-eabi/thumb");
		helper.addIncludeFolder(projectDescriptor, armThumb);

		contribEnv.addVariable(new EnvironmentVariable(ARM_NONE_EABI_THUMB, armThumb.toPortableString()), configuration);

		helper.addIncludeFolder(projectDescriptor, armIncl.append("backward"));
		IPath libGcc = compilerLocation.append("lib/gcc/arm-none-eabi/4.9.3");
		helper.addIncludeFolder(projectDescriptor, libGcc.append("include"));
		helper.addIncludeFolder(projectDescriptor, libGcc.append("include-fixed"));
	}
	
	@Override
	String getName() {
		return "GCC";
	}

}
