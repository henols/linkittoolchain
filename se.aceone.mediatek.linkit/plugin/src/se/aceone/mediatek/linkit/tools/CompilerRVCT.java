package se.aceone.mediatek.linkit.tools;

import org.eclipse.cdt.core.envvar.IContributedEnvironment;
import org.eclipse.cdt.core.settings.model.ICConfigurationDescription;
import org.eclipse.cdt.core.settings.model.ICProjectDescription;
import org.eclipse.cdt.core.settings.model.ICSettingEntry;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;

import se.aceone.mediatek.linkit.common.LinkItConst;

public class CompilerRVCT extends Compiler implements LinkItConst {
	public static final String COMPILER_IT_SDK10_RTVC = "RVCT31BIN";

	@Override
	public String getCompilerPath() {
		String compiler = System.getenv().get(COMPILER_IT_SDK10_RTVC);
		if (compiler == null) {
			compiler = "C:/Program Files/ARM/RVCT";
		} else {
			compiler = new Path(compiler).removeLastSegments(4).toPortableString();
		}
		return compiler;
	}

	@Override
	public void setMacros(ICProjectDescription projectDescription) {
		helper.addMacro(projectDescription, "__COMPILER_RVCT__", null, ICSettingEntry.BUILTIN);
	}

	@Override
	protected void setIncludePaths(ICProjectDescription projectDescriptor, IContributedEnvironment contribEnv, ICConfigurationDescription configuration) {
		IPath compilerLocation = new Path(getCompilerPath());
		IPath rvtcIncl = compilerLocation.append("Data/3.1/569/include/windows");
		helper.addIncludeFolder(projectDescriptor, rvtcIncl);
	}

	@Override
	protected String getToolChainId() {
		switch (helper.getOutputType().intValue()) {
		case 0: // VXP
			return LINKIT_DEFAULT_TOOL_CHAIN_RVCT;
		case 3: // Static
			return LINKIT_DEFAULT_TOOL_CHAIN_STATIC_RVCT;
		}
		return null;
	}

	@Override
	String getToolPath() {
		return new Path(COMPILER_TOOL_PATH_RVTC).toPortableString();
	}

	@Override
	String getName() {
		return "RVCT";
	}
}
