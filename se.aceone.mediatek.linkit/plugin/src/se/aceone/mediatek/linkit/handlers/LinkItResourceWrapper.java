/*
 * LinkIt Tool Chain, an eclipse plugin for LinkIt SDK 1.0 and 2.0
 * 
 * Copyright © 2015 Henrik Olsson (henols@gmail.com)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package se.aceone.mediatek.linkit.handlers;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.eclipse.cdt.core.CCorePlugin;
import org.eclipse.cdt.core.envvar.IContributedEnvironment;
import org.eclipse.cdt.core.envvar.IEnvironmentVariable;
import org.eclipse.cdt.core.envvar.IEnvironmentVariableManager;
import org.eclipse.cdt.core.settings.model.ICConfigurationDescription;
import org.eclipse.cdt.core.settings.model.ICProjectDescription;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.MessageConsole;

import se.aceone.mediatek.linkit.common.ExternalCommandLauncher;
import se.aceone.mediatek.linkit.common.LinkItConst;
import se.aceone.mediatek.linkit.tools.Common;
import se.aceone.mediatek.linkit.xml.config.Packageinfo;
import se.aceone.mediatek.linkit.xml.config.Packageinfo.APIAuth;
import se.aceone.mediatek.linkit.xml.config.Packageinfo.Namelist;
import se.aceone.mediatek.linkit.xml.config.Packageinfo.Operationinfo;
import se.aceone.mediatek.linkit.xml.config.Packageinfo.Output;
import se.aceone.mediatek.linkit.xml.config.Packageinfo.Resolution;
import se.aceone.mediatek.linkit.xml.config.Packageinfo.Targetconfig;
import se.aceone.mediatek.linkit.xml.config.Packageinfo.Userinfo;
import se.aceone.mediatek.linkit.xml.config.Packageinfo.Vxp;

public class LinkItResourceWrapper {

	static LinkItResourceWrapper PACK_WRAPPER = null;
	MessageConsole console = null;

	private LinkItResourceWrapper() {
		// no constructor needed
	}

	static private LinkItResourceWrapper getResourceWrapper() {
		if (PACK_WRAPPER == null) {
			PACK_WRAPPER = new LinkItResourceWrapper();
		}
		return PACK_WRAPPER;
	}

	static public void pack(IProject project, String cConf) {
		getResourceWrapper().internalPack(project, cConf);
	}

	public static MessageConsole findConsole(String name) {
		ConsolePlugin plugin = ConsolePlugin.getDefault();
		IConsoleManager conMan = plugin.getConsoleManager();
		IConsole[] existing = conMan.getConsoles();
		for (int i = 0; i < existing.length; i++) {
			if (name.equals(existing[i].getName())) {
				return (MessageConsole) existing[i];
			}
		}
		// no console found, so create a new one
		MessageConsole myConsole = new MessageConsole(name, null);
		conMan.addConsoles(new IConsole[] { myConsole });
		return myConsole;
	}

	public void internalPack(final IProject project, String cConf) {

		// Check that we have a AVR Project
		try {
			if (project == null || !(project.hasNature(LinkItConst.Cnatureid) || project.hasNature(LinkItConst.CCnatureid))) {
				Common.log(new Status(IStatus.ERROR, LinkItConst.CORE_PLUGIN_ID, "The current selected project is not an C/C++ Project", null));
				return;
			}
		} catch (CoreException e) {
			// Log the Exception
			Common.log(new Status(IStatus.ERROR, LinkItConst.CORE_PLUGIN_ID, "Can't access project nature", e));
		}

		// String UpLoadTool = Common.getBuildEnvironmentVariable(project,
		// cConf, LinkItConst.ENV_KEY_upload_tool, "");
		// String MComPort = Common.getBuildEnvironmentVariable(project, cConf,
		// LinkItConst.ENV_KEY_JANTJE_COM_PORT, "");
		console = findConsole("LinkIt pack");
		// console.clearConsole();
		console.activate();

		executCommand(project, "Packing resources");

	}

	protected List<IPath> getFilesWithExt(final IProject project, final String ext) {
		final List<IPath> files = new ArrayList<IPath>();
		try {
			project.accept(new IResourceVisitor() {
				public boolean visit(IResource resource) {
					if (resource.getType() == IResource.FILE && resource.getName().endsWith(ext)) {
						files.add(resource.getProjectRelativePath());
						return true;
					}
					return true;
				}
			}, 1, false);
		} catch (CoreException e) {
		}
		return files;
	}

	protected List<String> buildPackResourceCommand(final IProject project) {
		List<String> command = new ArrayList<String>();

		String endsWith = ".vcproj";
		List<IPath> files = getFilesWithExt(project, endsWith);
		if (files.isEmpty()) {
			// errorStream.println("No *.vcproj file in project " + project.getName());
			return null;
		} else if (files.size() > 1) {
			// warnStream.println("More then one *.vcproj file in project: " + project.getName() + " using: " +
			// files.get(0).toOSString());
		}

		String projFile = project.getFile(files.get(0)).getLocation().toOSString();
		String toolPath = getEnvVariable(project, LinkItConst.TOOL_PATH);

		IPath packCmd = new Path(toolPath).append(getEnvVariable(project, LinkItConst.RES_EDITOR));
		command.add(setQuotes(packCmd.toOSString()));
		command.add("load");
		command.add(setQuotes(projFile));
		return command;
	}

	protected Packageinfo getConfig(final IProject project) {
		JAXBContext jaxbContext;
		Packageinfo packageinfo;
		File file = new File(project.getFile("config.xml").getLocationURI());
		try {
			jaxbContext = JAXBContext.newInstance(Packageinfo.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			packageinfo = (Packageinfo) jaxbUnmarshaller.unmarshal(file);
		} catch (JAXBException e) {
			// errorStream.println("Error reading config.xml file in project: " + project.getName());
			return null;
		}
		return packageinfo;
	}

	protected String setQuotes(String s) {
		return '"' + s + '"';
	}

	protected void executCommand(final IProject project, final String jobName) {
		// final MessageConsole console = this.console;
		Job job = new Job(jobName) {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				try {
					project.refreshLocal(1, monitor);

					if (isStaticLib(project)) {
						return Status.OK_STATUS;
					}


					IFolder armDir = project.getFolder("arm");
					boolean createdArmDir = false;
					if(!armDir.exists()){
						armDir.create(true, true, monitor);
						createdArmDir =true;
					}
					List<String> command = buildPackResourceCommand(project);
					int ret = runConsoledCommand(console, command, monitor, project);
					if(createdArmDir){
						armDir.delete(true, monitor);
					}
					if (ret != 0) {
						Common.log(new Status(IStatus.ERROR, LinkItConst.CORE_PLUGIN_ID, "Pack Resource, Command returned an error code: " + ret));
					} else {
					}
				} catch (IOException e) {
					Common.log(new Status(IStatus.ERROR, LinkItConst.CORE_PLUGIN_ID, jobName + ", Problem while executing command.", e));
					return Status.OK_STATUS;
				} catch (CoreException e) {
					// TODO Auto-generated catch block
					Common.log(new Status(IStatus.ERROR, LinkItConst.CORE_PLUGIN_ID, jobName + ", Failed to copy file " + project.getName() + ".axf", e));
					return Status.OK_STATUS;
				}
				try {
					project.refreshLocal(1, monitor);
					IFile defaultVxp = project.getFile(new Path("Default.vxp"));
					if (defaultVxp.exists()) {
						defaultVxp.delete(true, monitor);
						project.refreshLocal(1, monitor);
					}
					IFile projectVxp = project.getFile(new Path(project.getName() + ".vxp"));
					if (projectVxp.exists()) {
						projectVxp.move(defaultVxp.getFullPath(), true, monitor);

					}
					project.refreshLocal(2, monitor);
				} catch (CoreException e) {
					Common.log(new Status(IStatus.ERROR, LinkItConst.CORE_PLUGIN_ID, jobName + ", Failed to rename file " + project.getName() + ".vxp", e));
				}

				return Status.OK_STATUS;

			}

		};
		job.setRule(null);
		job.setPriority(Job.LONG);
		job.setUser(true);
		job.schedule();
	}

	private boolean isStaticLib(IProject project) {
		Packageinfo packageinfo = getConfig(project);
		if (packageinfo == null) {
			return false;
		}
		Output output = packageinfo.getOutput();
		if (output == null) {
			return false;
		}
		BigInteger type = output.getType();
		if (type == null) {
			return false;
		}
		return type.intValue() == 3;
	}

	protected int runConsoledCommand(MessageConsole console, List<String> command, IProgressMonitor monitor, IProject project) throws IOException {
		ExternalCommandLauncher launcher = new ExternalCommandLauncher(command, project);
		launcher.setConsole(console);
		launcher.redirectErrorStream(true);
		return launcher.launch(monitor);
	}

	protected String getEnvVariable(final IProject project, String varName) {
		IEnvironmentVariableManager envManager = CCorePlugin.getDefault().getBuildEnvironmentManager();
		ICProjectDescription projectDescription = CCorePlugin.getDefault().getProjectDescriptionManager().getProjectDescription(project);

		IContributedEnvironment contribEnv = envManager.getContributedEnvironment();
		ICConfigurationDescription defaultConfigDescription = projectDescription.getDefaultSettingConfiguration();

		IEnvironmentVariable variable = contribEnv.getVariable(varName, defaultConfigDescription);
		return variable.getValue();
	}

	protected String getConfigurationName(final IProject project) {
		ICProjectDescription projectDescription = CCorePlugin.getDefault().getProjectDescriptionManager().getProjectDescription(project);
		return projectDescription.getActiveConfiguration().getName();
	}

}
